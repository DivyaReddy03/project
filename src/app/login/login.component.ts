import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import UserService from '../user.service';



@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    user:any;
  data

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private service:UserService
        
    ) {
        // redirect to home if already logged in
     
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;
  
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }else{
          this.user={
            username:this.loginForm.controls['username'].value,
            password:this.loginForm.controls['password'].value
          }
          console.log('hiii')
        this.loading = true;
        this.service.fetchUserByName(this.user.username,(data)=>{
          this.data=JSON.parse(data)[0]
        
          console.log(this.data.password)
      
          console.log(this.user.password)
      
          console.log('hello')
          if(this.data.password==this.user.password){
            console.log('checking')
            this.router.navigate(['/home'])
          }else{
            alert('u enetered wrong')
          }
        })
      }
      
    }
}
